import Stats from 'stats.js';

import Draw from '../lib/draw';

import Controls from './controls';
import Scene from './scene';
import Player from '../entity/player';

import CameraDefault from './camera-default';

export default class Game {
    isRunning = false;

    canvasWidth = window.innerWidth;
    canvasHeight = window.innerHeight;

    lastTime = 0;

    constructor(canvas){
        this.canvas = canvas;
        this.ctx = this.canvas.getContext('2d');

        this.camera = new CameraDefault(this);
        this.player = new Player(this);
        this.scene = new Scene(this);
        this.controls = new Controls(this);
        this.draw = new Draw(this);

        this.stats = new Stats();
        document.body.appendChild(this.stats.dom);

        this.initCanvas();
        this.scene.buildRandom(16);
        this.player.placeInOutsideField();
    }

    initCanvas(){
        this.canvas.width = this.canvasWidth;
        this.canvas.height = this.canvasHeight;
        this.canvas.onclick = () => this.canvas.requestPointerLock();
    }

    start(){
        this.isRunning = true;
        this.lastTime = 0;
        this.stats.showPanel(0);
        this.loop();
    }

    stop(){
        this.isRunning = false;
    }

    loop(time){
        if(!this.isRunning){
            return;
        }

        this.stats.begin();

        let deltaSeconds = 0;
        if(time && this.lastTime){
            deltaSeconds = (time - this.lastTime) / 1000;
        }
        this.lastTime = time;

        this.scene.update(deltaSeconds);
        this.player.update(deltaSeconds);
        this.camera.draw();

        this.stats.end();
        window.requestAnimationFrame(this.loop.bind(this));
    }
}
