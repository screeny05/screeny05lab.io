import Vector2 from '../lib/vector2';
import Random from '../lib/random';
import MathE from '../lib/math-e';
import Physics from '../engine/physics';

export default class Entity {
    hasCollisions = false;
    physics = false;
    _direction = 0;
    minDistanceBetweenEntities = 0;
    polygons = [];

    set direction(val){
        this._direction = MathE.wrapAngleRad(val);
    }

    get direction(){
        return this._direction;
    }


    constructor(game, position = new Vector2(), parent = null){
        this.game = game;
        this.position = position;
        this.parent = parent;
    }


    placeInOutsideField(){
        this.placeCentered(new Vector2(-1, -1));
    }

    placeInRandomField(){
        const emptyFields = this.game.scene.entities.filter(wall => wall.type === 0);
        this.placeCentered(Random.fromArray(emptyFields).position);
    }

    placeInCenterField(){
        this.placeCentered(new Vector2(0, 0));
    }

    placeCentered(position){
        this.position = position.floor().add(.5, .5);
    }

    /**
     * TODO: normalize multiple vectors to prevent faster diagonal moving
     */
    move(distance, direction){
        const newPosition = this.position.add(Vector2.fromPolar(distance, direction));

        if(!this.hasCollisions){
            this.position = newPosition;
            return;
        }

        const wallAtPositionX = this.game.scene.getEntityAt(new Vector2(newPosition.x, this.position.y));
        const wallAtPositionY = this.game.scene.getEntityAt(new Vector2(this.position.x, newPosition.y));

        if(!wallAtPositionX){
            this.position.x = newPosition.x;
        }

        if(!wallAtPositionY){
            this.position.y = newPosition.y;
        }
    }

    update(delta){
        if(this.physics){
            Physics.transformEntity(this);
        }
    }
}
