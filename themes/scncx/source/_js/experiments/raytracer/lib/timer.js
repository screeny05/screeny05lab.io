export default class Timer {
    constructor(duration, callback){
        this.duration = duration;
        this.callback = callback;
    }
}
