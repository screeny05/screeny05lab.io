export default class MathE {
    static PI2 = Math.PI * 2;
    static PI_HALF = Math.PI / 2;
    static DEG_TO_RAD_FACTOR = Math.PI / 180;
    static RAD_TO_DEG_FACTOR = 180 / Math.PI;
    static EPSILON = 0.000001;

    static degToRad(deg){
        return deg * MathE.DEG_TO_RAD_FACTOR;
    }

    static radToDeg(rad){
        return rad * MathE.RAD_TO_DEG_FACTOR;
    }

    static clamp(val, min, max){
        return Math.min(Math.max(val, min), max);
    }

    static wrap(val, min, max){
        let range = max - min;
        if(range <= 0){
            return 0;
        }
        let result = (val - min) % range;
        if(result < 0){
            result += range;
        }
        return result + min;
    }

    static wrapAngleRad(val){
        return MathE.wrap(val, 0, MathE.PI2);
    }

    static angleBetweenAngles(val1, val2){
        return Math.atan2(Math.sin(val1 - val2), Math.cos(val1 - val2));
    }

    static isWithin(val, min, max){
        return val <= max && val >= min;
    }

    static isWrappingWithin(val, min, max, wrap){
        val = (wrap + (val % wrap)) % wrap;
        min = (wrap * 10000 + min) % wrap;
        max = (wrap * 10000 + max) % wrap;
        if(min < max){
            return min <= val && val <= max;
        } else {
            return min <= val || val <= max;
        }
    }

    static isAngleWithin(val, min, max){
        return MathE.isWrappingWithin(val, min, max, MathE.PI2);
    }

    static lerp(start, end, t){
        return start + (end - start) * t;
    }

    static isNear(val, target, threshold = MathE.EPSILON){
        return val < target ? val + threshold >= target : val - threshold <= target;
    }

    static norm(val, min, max){
        return (val - min) / (max - min);
    }

    static map(val, min1, max1, min2, max2){
        return MathE.lerp(min2, max2, MathE.norm(val, min1, max1));
    }

    static round(val, digits = 0){
        if(digits === 0){
            return Math.round(number);
        }

        const exponent = 10 ** (digits + 1);
        return Math.round(number * exponent) / exponent;
    }
}
