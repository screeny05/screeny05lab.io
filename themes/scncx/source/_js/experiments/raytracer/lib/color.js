import MathE from './math-e';
import StringE from './string-e';

export default class Color {
    _r = 0;
    _g = 0;
    _b = 0;
    _a = 1;

    set r(val){
        this._r = MathE.clamp(Math.round(val), 0, 255);
    }

    get r(){
        return this._r;
    }

    set g(val){
        this._g = MathE.clamp(Math.round(val), 0, 255);
    }

    get g(){
        return this._g;
    }

    set b(val){
        this._b = MathE.clamp(Math.round(val), 0, 255);
    }

    get b(){
        return this._b;
    }

    set a(val){
        this._a = MathE.clamp(val, 0, 1);
    }

    get a(){
        return this._a;
    }


    constructor(r = 0, g = 0, b = 0, a = 1){
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    toHex(){
        return `#${Color.formatAsHex(this.r)}${Color.formatAsHex(this.g)}${Color.formatAsHex(this.b)}${this.a !== 1 ? Color.formatAsHex(this.a * 255) : ''}`;
    }

    toRGBA(){
        return `rgba(${this.r}, ${this.g}, ${this.b}, ${this.a})`;
    }

    equals(color){
        return this.r === color.r && this.g === color.g && this.b === color.b && this.a === color.a;
    }

    get [Symbol.toStringTag](){
        return this.toRGBA();
    }

    static formatAsHex(val){
        return StringE.leftPad(Math.round(val).toString(16), 2);
    }
}
