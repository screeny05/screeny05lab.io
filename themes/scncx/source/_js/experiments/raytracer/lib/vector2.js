import MathE from './math-e';

export default class Vector2 {
    x = 0;
    y = 0;

    constructor(x = 0, y = 0){
        this.x = x;
        this.y = y;
    }

    add(a, b){
        if(Vector2.isVector(a)){
            return new Vector2(this.x + a.x, this.y + a.y);
        }

        return new Vector2(this.x + a, this.y + b);
    }

    subtract(v){
        return new Vector2(this.x - v.x, this.y - v.y);
    }

    multiply(scaleX, scaleY = scaleX){
        return new Vector2(this.x * scaleX, this.y * scaleY);
    }

    divide(divisor){
        return new Vector2(this.x / divisor, this.y / divisor);
    }

    floor(){
        return new Vector2(Math.floor(this.x), Math.floor(this.y));
    }

    ceil(){
        return new Vector2(Math.ceil(this.x), Math.ceil(this.y));
    }

    equals(v){
        return this.x === v.x && this.y === v.y;
    }

    dot(v){
        return (this.x * v.x) + (this.y * v.y);
    }

    normalize(){
        const magnitude = this.getMagnitude();
        return new Vector2(this.x / magnitude, this.y / magnitude);
    }

    angleToVector(v){
        return Math.atan2(this.y - v.y, this.x - v.x);
    }

    toPolar(){
        return Math.atan2(this.y, this.x);
    }

    moveTowardsVector(to, maxDistanceDelta){
        return to
            .subtract(this)
            .normalize()
            .multiply(maxDistanceDelta)
            .add(this);
    }

    lerp(to, t){
        return new Vector2(MathE.lerp(this.x, to.x, t), MathE.lerp(this.y, to.y, t));
    }

    getMagnitude(){
        return Math.sqrt(this.getSqrMagnitude());
    }

    getSqrMagnitude(){
        return (this.x * this.x) + (this.y * this.y);
    }

    get [Symbol.toStringTag](){
        return `Vector2(${this.x}, ${this.y})`;
    }

    static fromIndex(i, size){
        return new Vector2(i % size, Math.floor(i / size));
    }

    static fromPolar(length, angle){
        return new Vector2(length * Math.cos(angle), length * Math.sin(angle));
    }

    static isVector(v){
        return typeof v === 'object' && typeof v.x === 'number' && typeof v.y === 'number';
    }

    static getAverageMagnitude(vectors){
        let average = 0;
        vectors.forEach(v => average += v.getMagnitude());
        return average / vectors.length;
    }

    static getAverageRelativeMagnitude(origin, vectors){
        return Vector2.getAverageMagnitude(vectors.map(v => v.subtract(origin)));
    }

    static getAverageVector(vectors){
        if(vectors.length === 0){
            return new Vector2();
        }

        let averageVector = vectors[0];
        vectors.slice(1).forEach(v => averageVector = averageVector.add(v));
        return averageVector.divide(vectors.length);
    }

    static getAverageRelativeVector(origin, vectors){
        return Vector2.getAverageVector(vectors).subtract(origin);
    }

    static getNearestVector(origin, vectors){
        return vectors
            .map(v => v.subtract(origin))
            .sort((a, b) => a.getSqrMagnitude() - b.getSqrMagnitude())[0];
    }


    static forward(){
        return new Vector2(0, 0);
    }
    static back(){
        return new Vector2(0, 0);
    }
    static left(){
        return new Vector2(-1, 0);
    }
    static right(){
        return new Vector2(1, 0);
    }
    static up(){
        return new Vector2(0, 1);
    }
    static down(){
        return new Vector2(0, -1);
    }
    static one(){
        return new Vector2(1, 1);
    }
    static zero(){
        return new Vector2(0, 0);
    }
    static infinity(){
        return new Vector2(Infinity, Infinity);
    }
    static negativeInfinity(){
        return new Vector2(-Infinity, -Infinity);
    }
}
