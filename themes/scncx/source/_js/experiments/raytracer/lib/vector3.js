export default class Vector3 {
    x = 0;
    y = 0;
    z = 0;

    constructor(x = 0, y = 0, z = 0){
        if(x instanceof Vector2){
            this.x = x.x;
            this.y = x.y;
            this.z = y;
        } else {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    add(v){
        return new Vector3(this.x + v.x, this.y + v.y, this.z + v.z);
    }

    subtract(v){
        return new Vector3(this.x - v.x, this.y - v.y, this.z - v.z);
    }

    multiply(scaleX, scaleY = scaleX, scaleZ = scaleY){
        return new Vector3(this.x * scaleX, this.y * scaleY, this.z * scaleZ);
    }

    divide(divisor){
        return new Vector3(this.x / divisor, this.y / divisor, this.z / divisor);
    }

    floor(){
        return new Vector3(Math.floor(this.x), Math.floor(this.y), Math.floor(this.z));
    }

    ceil(){
        return new Vector3(Math.ceil(this.x), Math.ceil(this.y), Math.ceil(this.z));
    }

    equals(v){
        return this.x === v.x && this.y === v.y && this.z === v.z;
    }

    dot(v){
        return (this.x * v.x) + (this.y * v.y) + (this.z * v.z);
    }

    normalize(){
        const magnitude = this.getMagnitude();
        return new Vector2(this.x / magnitude, this.y / magnitude, this.z / magnitude);
    }

    moveTowardsVector(to, maxDistanceDelta){
        return to
            .subtract(this)
            .normalize()
            .multiply(maxDistanceDelta)
            .add(this);
    }

    lerp(to, t){
        return new Vector3(MathE.lerp(this.x, to.x, t), MathE.lerp(this.y, to.y, t), MathE.lerp(this.z, to.z, t));
    }

    getMagnitude(){
        return Math.sqrt(this.getSqrMagnitude());
    }

    getSqrMagnitude(){
        return (this.x * this.x) + (this.y * this.y) + (this.z * this.z);
    }

    get [Symbol.toStringTag](){
        return `Vector3(${this.x}, ${this.y}, ${this.z})`;
    }
}
