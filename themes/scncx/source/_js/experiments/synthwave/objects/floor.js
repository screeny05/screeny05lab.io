import GameObject from './base';
import * as THREE from 'three';
import FSNoise from 'fast-simplex-noise';

export default class FloorObject extends GameObject {
    material = null;
    noiseFloor = null;
    plainFloor = null;

    constructor(){
        super();
        //this.initPlainFloor();
        this.initNoiseFloor();
    }

    initPlainFloor(){
        const texture = new THREE.TextureLoader().load('/images/retro-grid.jpg');
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set(128, 128);
        texture.offset.set(0.5, 0.5);
        const material = new THREE.MeshBasicMaterial({
            color: 0xffffff,
            map: texture
        });
        this.plainFloor = new THREE.Mesh(new THREE.PlaneBufferGeometry(10000, 10000), material);
        this.plainFloor.rotation.x = Math.PI / -2;
        this.plainFloor.position.y = -0.5;
    }

    initNoiseFloor(){
        const segments = 64;
        const texture = new THREE.TextureLoader().load('/images/retro-grid.jpg');
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;
        texture.repeat.set(segments, segments);
        texture.offset.set(0.5, 0.5);
        const material = new THREE.MeshPhongMaterial({
            color: 0xffffff,
            shading: THREE.FlatShading,
            map: texture,
            shininess: 0
        });
        this.noiseFloor = new THREE.Mesh(this.getNoiseGeometry(10000, 10000, segments), material);
        this.noiseFloor.rotation.x = Math.PI / -2;

        this.noiseFloor.receiveShadow = true;
    }

    getNoiseGeometry(width, height, segments){
        const geometry = new THREE.PlaneGeometry(width, height, segments, segments);
        const noise = new FSNoise({ max: 40, min: 0 });
        geometry.vertices.forEach((v, i) => v.z = noise.scaled2D(Math.floor(i / 32), i % 32));
        return geometry;
    }

    onAdd(game){
        super.onAdd(game);
        this.game.scene.add(this.plainFloor);
        this.game.scene.add(this.noiseFloor);
    }

    update(delta){
        //this.mesh.position.z += 0.1;
    }
}
