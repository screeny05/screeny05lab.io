import GameObject from './base';
import Controls from '../engine/controls';

export default class CameraControls extends GameObject {
    controls = null;
    moveSpeed = 200;
    rotationSpeed = 2;

    onAdd(game){
        super.onAdd(game);
        this.controls = new Controls(this.game.container);

        // use world rotation
        this.game.camera.rotation.order = 'ZYX';
        this.game.camera.position.set(0, 100, 0);
    }

    update(delta){
        if(this.controls.states.forward > 0){
            this.game.camera.translateZ(this.controls.states.forward * delta * -this.moveSpeed);
        }
        if(this.controls.states.backward > 0){
            this.game.camera.translateZ(this.controls.states.backward * delta * this.moveSpeed);
        }
        if(this.controls.states.left > 0){
            this.game.camera.translateX(this.controls.states.left * delta * -this.moveSpeed);
        }
        if(this.controls.states.right > 0){
            this.game.camera.translateX(this.controls.states.right * delta * this.moveSpeed);
        }
        if(this.controls.states.horizontal !== 0){
            this.game.camera.rotation.y += this.controls.states.horizontal * delta * this.rotationSpeed;
        }

        const newVertical = this.game.camera.rotation.x + this.controls.states.vertical * delta * this.rotationSpeed;
        if(this.controls.states.vertical !== 0 && Math.abs(newVertical) < Math.PI / 2){
            this.game.camera.rotation.x = newVertical;
        }
    }
}
