import GameObject from './base';
import * as THREE from 'three';

export default class PyramidObject extends GameObject {
    material = null;
    mesh = null;

    constructor(){
        super();

        this.material = new THREE.MeshPhongMaterial({
            color: 0xDA1192,
            shading: THREE.FlatShading,
            shininess: 0
        });
        this.mesh = new THREE.Mesh(new THREE.TetrahedronBufferGeometry(50, 0), this.material);
        this.mesh.position.z = -250;
        this.mesh.position.y = 100;

        this.mesh.castShadow = true;
    }

    update(delta){
        this.mesh.rotation.x += 0.5 * delta;
        this.mesh.rotation.y += 0.5 * delta;
    }

    onAdd(game){
        super.onAdd(game);
        this.game.scene.add(this.mesh);
    }
}
