import GameObject from './base';
import * as THREE from 'three';

export default class Light extends GameObject {
    lights = [];

    constructor(){
        super();
        this.initHemisphereLight();
        this.initPointLight();
    }

    initHemisphereLight(){
        const light = new THREE.HemisphereLight(0x8e2e64, 0x00171d, 1);
        this.lights.push(light);
    }

    initPointLight(){
        const light = new THREE.PointLight(0xDA1192, 1, 0);
        light.position.set(0, 500, 0);
        light.castShadow = true;
        this.lights.push(light);
    }

    onAdd(game){
        super.onAdd(game);
        this.lights.forEach(light => game.scene.add(light));
    }
}
