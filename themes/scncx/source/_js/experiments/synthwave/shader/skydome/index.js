import * as THREE from 'three';

import fragmentShader from './skydome.fs';
import vertexShader from './skydome.vs';

export default {
    uniforms: {
        topColor: { value: new THREE.Color(0x221c31) },
        bottomColor: { value: new THREE.Color(0x000000) },
        offset: { value: 33 },
        exponent: { value: 0.3 }
    },

    vertexShader,
    fragmentShader
};
