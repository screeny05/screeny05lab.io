import * as mathE from './core.js';
import * as random from './random.js';

export default class Vector2 {
    constructor(x = 0, y = 0){
        this.x = x;
        this.y = y;
    }

    /* operators */
    add(value){
        this.x += value.x;
        this.y += value.y;
        return this;
    }

    subtract(value){
        this.x -= value.x;
        this.y -= value.y;
        return this;
    }

    modulate(value){
        this.x *= value.x;
        this.y *= value.y;
        return this;
    }

    multiply(scale){
        this.x *= scale;
        this.y *= scale;
        return this;
    }

    divide(scale){
        this.x /= scale;
        this.y /= scale;
        return this;
    }

    negate(){
        this.x = -this.x;
        this.y = -this.y;
        return this;
    }

    /* vector operations */
    addVector(value){
        this.x += value.x;
        this.y += value.y;
        return this;
    }

    equals(value){
        return this.x == value.x && this.y == value.y;
    }

    distanceTo(value){
        return value.subtract(this).length;
    }

    // use for distance-comparision with performance gain
    sqrDistanceTo(value){
        return value.subtract(this).sqrLength;
    }

    /* non-mutating methods */
    clamp(min, max){
        const x = mathE.clamp(this.x, min.x, max.x);
        const y = mathE.clamp(this.y, min.y, max.y);
        return new Vector2(x, y);
    }

    normalize(){
        const length = this.length;
        const x = this.x / length;
        const y = this.y / length;
        return new Vector2(x, y);
    }

    lerp(to, factor, lerpfn){
        factor = lerpfn(factor);
        const x = mathE.lerp(this.x, to.x, factor);
        const y = mathE.lerp(this.y, to.y, factor);
        return new Vector2(x, y);
    }

    moveTowards(to, maxDistanceDelta){
        return to
            .clone()
            .subtract(this)
            .normalize()
            .multiply(maxDistanceDelta)
            .add(this);
    }

    dot(value){
        return (this.x * value.x) + (this.y * value.y);
    }

    reflect(normal){
        const dot = this.dot(normal);
        const x = this.x - ((2 * dot) * normal.x);
        const y = this.y - ((2 * dot) * normal.y);
        return new Vector2(x, y);
    }

    randomAround(distance){
        const x = random.between(this.x - distance, this.x + distance);
        const y = random.between(this.y - distance, this.y + distance);
        return new Vector2(x, y);
    }

    clone(){
        return new Vector2(this.x, this.y);
    }

    [Symbol.toStringTag](){
        return `[object Vector2(${this.x}, ${this.y})]`;
    }

    minimize(value){
        const x = Math.min(this.x, value.x);
        const y = Math.min(this.y, value.y);
        return new Vector2(x, y);
    }

    maximize(value){
        const x = Math.max(this.x, value.x);
        const y = Math.max(this.y, value.y);
        return new Vector2(x, y);
    }


    /* properties */
    get length(){
        return Math.sqrt(this.sqrLength);
    }

    // use for length-comparision with performance gain
    get sqrLength(){
        return (this.x * this.x) + (this.y * this.y);
    }


    /* static functions */
    static randomXY(min = Vector2.zero(), max = Vector2.one()){
        const x = random.between(min.x, max.x, false);
        const y = random.between(min.y, max.y, false);
        return new Vector2(x, y);
    }


    static forward(){
        return new Vector2(0, 0);
    }
    static back(){
        return new Vector2(0, 0);
    }
    static left(){
        return new Vector2(-1, 0);
    }
    static right(){
        return new Vector2(1, 0);
    }
    static up(){
        return new Vector2(0, 1);
    }
    static down(){
        return new Vector2(0, -1);
    }
    static one(){
        return new Vector2(1, 1);
    }
    static zero(){
        return new Vector2(0, 0);
    }
    static infinity(){
        return new Vector2(Infinity, Infinity);
    }
    static negativeInfinity(){
        return new Vector2(-Infinity, -Infinity);
    }
}
