export const PI2 = Math.PI * 2;
export const DEG2RADF = Math.PI / 180;
export const RAD2DEGF = 180 / Math.PI;

export function roundPrecise(number, digits){
    if(digits === 0){
        return Math.round(number);
    }

    const exponent = 10 ** (digits + 1);
    return Math.round(number * exponent) / exponent;
};


export function clamp(val, min, max){
    return Math.max(Math.min(value, max), min);
};


export function loop(val, min, max){
    return val < min ? max : (val > max ? min : val);
};


export function wrap(val, min, max){
    const range = max - min;
    if(range <= 0){
        return 0;
    }

    let result = (val - min) % range;
    if(result < 0){
        result += range;
    }

    return result + min;
};


export function isOdd(val){
    return val % 2 !== 0;
};


export function isEven(val){
    return val % 2 === 0;
};


export function isNear(val, target, treshold){
    return val < target ? val + treshold >= target : val - treshold <= target;
};


export function isWithin(val, min, max){
    return val <= max && val >= min;
};


export function lerp(start, end, t){
    return start + (end - start) * t;
};


export function norm(val, min, max){
    return (val - min) / (max - min);
};


export function map(val, min1, max1, min2, max2){
    return lerp(min2, max2, norm(val, min1, max1));
};


export function countSteps(val, step, overflow){
    val = Math.floor(val / step);

    if(overflow){
        return val % overflow;
    }

    return val;
};


/**
 * returns the average between multiple values
 * @param  {rest<number>} ...values
 * @return {number}                 average
 */
export function average(...values){
    return values.reduce((prev, cur) => prev + cur) / values.length;
};


export function shear(val){
    return val % 1;
};


export function snapTo(val, gap, offset = 0){
    if(gap === 0){
        return val;
    }

    val -= offset;
    val = gap * Math.round(val / gap);

    return val + offset;
};


export function degTorad(deg){
    return deg * DEG2RADF;
};


export function radTodeg(rad){
    return rad * RAD2DEGF;
};
